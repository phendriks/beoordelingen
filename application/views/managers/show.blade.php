<!doctype html>
<html lang="en" class="no-js">
<head>
<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Managers - Beoordelingen - KPN</title>
<meta name="description" content="">
<meta name="author" content="">

<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="/apple-touch-icon.png">

{{ HTML::style('css/style.css') }}
{{ HTML::style('css/base.css') }}
{{ HTML::style('css/grid.css') }}

{{ HTML::style('css/themes/light.css') }}
{{ HTML::style('css/themes/green.css') }}
{{ HTML::style('js/themes/metro/jtable_metro_base.min.css') }}
{{ HTML::style('//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/flick/jquery-ui.min.css') }}
{{ HTML::style('js/themes/metro/green/jtable.min.css') }}
{{ HTML::script('//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js') }}
{{ HTML::script('//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js') }}
{{ HTML::script('js/jquery.jtable.min.js') }}
{{ HTML::script('js/localization/jquery.jtable.nl-NL.js') }}
<script type="text/javascript">
    $(document).ready(function () {

        $('#managerstablecontainer').jtable({
            title: 'Managers',
            paging: true, 
            pageSize: 10,
            ajaxSettings: { type: 'POST' },

            actions: {
                listAction: '{{ URL::to("/manager/list") }}',
                createAction: '{{ URL::to("/manager/add") }}',
                updateAction: '{{ URL::to("/manager/update") }}',
                deleteAction: '{{ URL::to("/manager/delete") }}'
            },
            fields: {
                id: {
                    key: true,
                    list: false,
                    create: false,
                    edit: false
                },
                manager: {
                    title: 'Manager',
                    list: true,
                    edit: true,
                    create: true
                },
                rapport: {
                   width: "0.2%",
                   sorting: false,
                   edit: false,
                   create: false,
                   listClass: 'child-opener-image-column',
                   display: function (Data) {
                       var $img = $("<img class='child-opener-image' src='{{ URL::to('js/themes/metro/') }}list_metro.png' title='Rapport' />");
                       $img.click(function () {
                         location.href = '{{ URL::to("/scores/bereken/") }}'+Data.record.manager;
                       });
                       return $img;
                   }
                   }
                   }
        });
    $('#managerstablecontainer').jtable('load');
    });

</script>
</head>

<!--[if IE 7 ]>    <body class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <body class="ie8"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<body>



<!--<![endif]-->
<div id="wrapper" class="container_12"> 



<!-- start header -->
  <header> 
    <!-- logo -->
    <h1 id="logo"><a href="{{ URL::to('/') }}">KPN</a></h1>
    <!-- nav -->
       <nav>
<ul id="nav">
  <li><a href="{{ URL::to('/') }}">Dashboard</a></li>
  <li><a href="{{ URL::to('beoordeling') }}">Beoordelingen</a></li>
  <li class="current"><a href="{{ URL::to('manager') }}">Managers</a></li>

  <!-- <li><a href="{{ URL::to('uitloggen') }}">Uitloggen</a></li> -->
</ul>
<br class="cl" />
    </nav>
  <br class="cl" />  
  </header>
  
  <div id="page">
  
  <h2 class="ribbon">Managers</h2>

  <div class="triangle-ribbon"></div>
  <div id="grid">
  
    <div id="managerstablecontainer"></div>

  </div>
  </div>
    
  <footer>
    <ul class="footer-nav">
      <li><a href="{{ URL::to('/') }}">Dashboard</a> |</li>
      <li><a href="http://www.kpn.nl">KPN</a></li>
    </ul>
    <p>Copyright &copy;2014, <a href="http://www.danakool.nl">Dana Kool</a></p>
    <br class="cl" />
  </footer>
  
  
  <!--[if lt IE 7 ]>
    {{ HTML::script('js/dd_belatedpng.js') }}
  <![endif]--> 
</div>
</body>
</html>