<!doctype html>
<html lang="en" class="no-js">
<head>
<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Rapport {{ $naam  }} - Beoordelingen - KPN</title>
<meta name="description" content="">
<meta name="author" content="">

<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="/apple-touch-icon.png">

{{ HTML::style('css/style.css') }}
{{ HTML::style('css/base.css') }}
{{ HTML::style('css/grid.css') }}

{{ HTML::style('css/themes/light.css') }}
{{ HTML::style('css/themes/green.css') }}

{{ HTML::script('//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js') }}

{{ HTML::script('//www.flotcharts.org/flot/jquery.flot.js') }}
{{ HTML::script('//jumjum123.github.io/JUMFlot/javascripts/JUMFlot.min.js') }}

<script type="text/javascript">
    $(document).ready(function() {        
        var options = {
	series:{ 
        spider:{
            active: true,
            highlight: {mode: "area"},
            legs: { 
		data: [{label: "Houding"},{label: "Initiatief"},{label: "Communicatie"},{label: "Tech"},{label: "Kwaliteit"},{label: "Proces"},{label: "Extra"}],
                legScaleMax: 1,legScaleMin:0.8
			},
            spiderSize: 0.9        
		}
            },
            grid:{ 
            hoverable: true,
            tickColor: "rgba(0,0,0,0.2)",mode: "radar"
            }
        };
        
        var url = '{{  URL::to("rapport/chart")  }}/';
        var id = '{{ $id }}';
        
        $.get(url + id, function(data){   
            $.plot($("#chart"),data,options);
        });
        
});
</script>
<style type="text/css">
    @media print
    {
    #nav{
        display: none;
    }
    footer{
        display: none;
    }
    }
    @media all{
        th{
            text-align: left;
            font-size: 14px;
        }
    }
</style>
</head>

<!--[if IE 7 ]>    <body class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <body class="ie8"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<body>
<!--<![endif]-->
<div id="wrapper" class="container_12"> 



<!-- start header -->
  <header> 
    <!-- logo -->
    <h1 id="logo"><img id="logoprint" src="{{ URL::to('/') }}/img/logo.png"><a href="{{ URL::to('/') }}">KPN</a></h1>
    <!-- nav -->
       <nav>
<ul id="nav">
  <li><a href="{{ URL::to('/') }}">Dashboard</a></li>
  <li><a href="{{ URL::to('beoordeling') }}">Beoordelingen</a></li>
  <li><a href="{{ URL::to('manager') }}">Managers</a></li>

  <!-- <li><a href="{{ URL::to('uitloggen') }}">Uitloggen</a></li> -->
</ul>
<br class="cl" />
    </nav>
  <br class="cl" />  
  </header>
  
  <div id="page">
  <div id="prijs"></div>
  <h2 class="ribbon">Rapport {{ $naam }}</h2>

  <div class="triangle-ribbon"></div>
  <div id="grid">
      <div id="manager" style="margin-bottom:50px;">
          <span style="font-size: 19px; font-weight: bold">Manager: {{ $manager }}</span>
      </div>
      <table style="width:950px; margin-bottom: 40px;">
        <tr>
          <th>Beoordelingsdatum</th>
          <th>Houding en gedrag</th>		
          <th>Initiatief</th>
          <th>Communicatie</th>
          <th>Technische kennis</th>
          <th>Kwaliteit tickets</th>
          <th>Proces kennis</th>
          <th>Extra (Opt)</th>
          <th>Totaalscore</th>
        </tr>
        <tr>
            <?php
            function check($num){
                $return  = null;
                switch ($num):
                case 0: $return = "N.v.t."; break;
                case 1: $return = "<u>Slecht</u>"; break;
                case 2: $return = "<u>Onvoldoende</u>"; break;
                case 3: $return = "Voldoende"; break;
                case 4: $return = "Goed"; break;
                case 5: $return = "Zeer Goed"; break;
                default : $return = "Onbekend"; break;
                endswitch;
                return $return;
            }
            ?>
        @foreach ($beoordelingen as $beoordeling)
        <tr>
            <td>{{ $beoordeling->created_at }}</td>
            <td>{{ check($beoordeling->punt_1) }}</td>
            <td>{{ check($beoordeling->punt_2) }}</td>
            <td>{{ check($beoordeling->punt_3) }}</td>
            <td>{{ check($beoordeling->punt_4) }}</td>
            <td>{{ check($beoordeling->punt_5) }}</td>
            <td>{{ check($beoordeling->punt_6) }}</td>
            <td>{{ check($beoordeling->punt_7) }}</td>
            <td>{{ $beoordeling->totaalscore }}</td>
        </tr>
        @endforeach
        </table>
      <table id="opmerkingen" style="width: 950px; margin-bottom: 40px;">
          <tr>
              <th>Datum</th>
              <th>Toelichting</th>
          </tr>
          @foreach ($beoordelingen as $beoordeling)
            <tr>
              <td>{{ $beoordeling->created_at }}</td>
              <td>* {{ $beoordeling->verbeterpunt }}</td>
            </tr>
          @endforeach
      </table>
      <div id="chart" style="width:900px;height:500px; margin-left: 80px"></div>
  </div>
  </div>
    
  <footer>
    <ul class="footer-nav">
      <li><a href="{{ URL::to('/') }}">Dashboard</a> |</li>
      <li><a href="http://www.kpn.nl">KPN</a></li>
    </ul>
    <p>Copyright &copy;2014, <a href="http://www.danakool.nl">Dana Kool</a></p>
    <br class="cl" />
  </footer>
  
   
  
  <!--[if lt IE 7 ]>
    {{ HTML::script('js/dd_belatedpng.js') }}
  <![endif]--> 
</div>
</body>
</html>