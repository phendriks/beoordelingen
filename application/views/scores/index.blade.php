<!doctype html>
<html lang="en" class="no-js">
<head>
<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Scores {{ $manager }} - Beoordelingen - KPN</title>
<meta name="description" content="">
<meta name="author" content="">

<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="/apple-touch-icon.png">

{{ HTML::style('css/style.css') }}
{{ HTML::style('css/base.css') }}
{{ HTML::style('css/grid.css') }}

{{ HTML::style('css/themes/light.css') }}
{{ HTML::style('css/themes/green.css') }}

{{ HTML::script('//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js') }}


<style type="text/css">
    @media print
    {
    #nav{
        display: none;
    }
    footer{
        display: none;
    }
    }
    @media all{
        th{
            text-align: left;
            font-size: 14px;
        }
    }
    #personen > tbody:nth-child(1) > tr:nth-child(even){ background-color: #EDEDED;}
</style>
</head>

<!--[if IE 7 ]>    <body class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <body class="ie8"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<body>
<!--<![endif]-->
<div id="wrapper" class="container_12"> 



<!-- start header -->
  <header> 
    <!-- logo -->
    <h1 id="logo"><img id="logoprint" src="{{ URL::to('/') }}/img/logo.png"><a href="{{ URL::to('/') }}">KPN</a></h1>
    <!-- nav -->
       <nav>
<ul id="nav">
  <li><a href="{{ URL::to('/') }}">Dashboard</a></li>
  <li><a href="{{ URL::to('beoordeling') }}">Beoordelingen</a></li>
  <li><a href="{{ URL::to('manager') }}">Managers</a></li>

  <!-- <li><a href="{{ URL::to('uitloggen') }}">Uitloggen</a></li> -->
</ul>
<br class="cl" />
    </nav>
  <br class="cl" />  
  </header>
  
  <div id="page">
  <div id="prijs"></div>
  <h2 class="ribbon">Scores: {{ $manager }}</h2>

  <div class="triangle-ribbon"></div>
  <div id="grid">
      <div id="manager" style="margin-bottom:50px;">
          <span style="font-size: 19px; font-weight: bold">Manager: {{ $manager }}</span>
      </div>
      <table id="personen" style="width:950px; margin-bottom: 40px;">
        <tr>
          <th>Medewerker</th>
          <th>Beoordelingsdatum</th>
          <th>Totaalscore</th>
        </tr>
        @foreach ($data as $record)
        <tr>
            <td>{{ $record->naam }}</td>
            <td>{{ $record->beoordelingsdatum }}</td>
            <td>{{ round($record->totaal, 2) }}</td>
        </tr>
        @endforeach
        </table>
      <!--
      <table id="opmerkingen" style="width: 950px; margin-bottom: 40px;">
          <tr>
              <th>Datum</th>
              <th>Toelichting</th>
          </tr>
          
      </table>
      -->
      
  </div>
  </div>
    
  <footer>
    <ul class="footer-nav">
      <li><a href="{{ URL::to('/') }}">Dashboard</a> |</li>
      <li><a href="http://www.kpn.nl">KPN</a></li>
    </ul>
    <p>Copyright &copy;2014, <a href="http://www.danakool.nl">Dana Kool</a></p>
    <br class="cl" />
  </footer>
  
   
  
  <!--[if lt IE 7 ]>
    {{ HTML::script('js/dd_belatedpng.js') }}
  <![endif]--> 
</div>
</body>
</html>