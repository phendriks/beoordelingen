<!doctype html>
<html lang="en" class="no-js">
<head>
<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Overzicht - Beoordelingen - KPN</title>
<meta name="description" content="">
<meta name="author" content="">

<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="/apple-touch-icon.png">

{{ HTML::style('css/style.css') }}
{{ HTML::style('css/base.css') }}
{{ HTML::style('css/grid.css') }}

{{ HTML::style('css/themes/light.css') }}
{{ HTML::style('css/themes/green.css') }}
{{ HTML::style('js/themes/metro/jtable_metro_base.min.css') }}
{{ HTML::style('//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/flick/jquery-ui.min.css') }}
{{ HTML::style('js/themes/metro/green/jtable.min.css') }}
{{ HTML::script('//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js') }}
{{ HTML::script('//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js') }}
{{ HTML::script('js/jquery.jtable.min.js') }}
{{ HTML::script('js/localization/jquery.jtable.nl-NL.js') }}
<script type="text/javascript">
    $(document).ready(function () {
        $('#knopje').click(function(){
          $('#medewerkerstablecontainer').jtable('load', { naam: $('#naam').val() });
        });
        $('#medewerkerstablecontainer').jtable({
            title: 'Medewerkers',
            paging: true, 
            pageSize: 10, 
            sorting: true,
            ajaxSettings: { type: 'POST' },
            defaultSorting: 'naam ASC',

            actions: {
                listAction: '{{ URL::to("/medewerker/list") }}',
                createAction: '{{ URL::to("/medewerker/add") }}',
                updateAction: '{{ URL::to("/medewerker/update") }}',
                deleteAction: '{{ URL::to("/medewerker/delete") }}'
            },
            fields: {
                id: {
                    key: true,
                    list: false,
                    create: false,
                    edit: false
                },
                naam: {
                    title: 'Naam',
		    display: function (data) {
        		return '<b>'+data.record.naam+'</b>';
    		}
                },
                manager: {
                    title: 'Manager',
                    list: true,
                    edit: true,
                    create: true,
                    options : '{{ URL::to("/medewerker/managers") }}'
                },
                beoordelingen: {
                    width: "1%",
                    sorting: false,
                    edit: false,
                    create: false,
                    listClass: 'child-opener-image-column',
                    display: function (medewerkerData) {
                        //Create an image that will be used to open child table
                        var $img = $("<img class='child-opener-image' src='{{ URL::to('js/themes/metro/') }}list_metro.png' title='Beoordelingen' />");
                        //Open child table when user clicks the image
                        $img.click(function () {
                            $('#medewerkerstablecontainer').jtable('openChildTable',
                                    $img.closest('tr'), //Parent row
                                    {
                                        title:  'Beoordelingen van: '+ medewerkerData.record.naam,
                                        pageSize: 10,
                                        actions: {
                                            listAction: '{{ URL::to("/beoordeling/list") }}/'+ medewerkerData.record.id,
                                            deleteAction: '{{ URL::to("/beoordeling/delete") }}',
                                            updateAction: '{{ URL::to("/beoordeling/update") }}',
                                            createAction: '{{ URL::to("/beoordeling/add") }}/'+ medewerkerData.record.id
                                        },
                                        toolbar: {
                                                    items: [{
                                                        tooltip: 'Rapport uitdraaien',
                                                        text: 'Rapport uitdraaien',
                                                        click: function () {
                                                            var url = '{{ URL::to("/rapport/verwerk") }}/';
                                                            location.href = url + medewerkerData.record.manager + "/" + medewerkerData.record.id;
                                                        }
                                                    }]
                                                },
                                        fields: {
                                            id: {
                                                key: true,
                                                create: false,
                                                edit: false,
                                                list: false
                                            },
                                            created_at: {
                                              list: true,
                                              edit: false,
                                              create: false,
                                              title: 'Beoordelings Datum',
                                              width: "20%"
                                            },
                                            medewerker_id: {
                                                key: false,
                                                create: false,
                                                edit: false,
                                                list: false
                                                },
                                            punt_1: {
                                                title: 'Houding en Gedrag',
                                                width: '19%',
                                                options: {1: "<span style='color:red;'>Slecht</span>", 2: "<span style='color:red;'>Onvoldoende</span>", 3: "Voldoende", 4: "Goed", 5: "Zeer Goed"}
                                            },
                                            punt_2: {
                                                title: 'Initiatief',
                                                width: '5%',
                                                options: {1: "<span style='color:red;'>Slecht</span>", 2: "<span style='color:red;'>Onvoldoende</span>", 3: "Voldoende", 4: "Goed", 5: "Zeer Goed"}
                                            },
                                            punt_3: {
                                                title: 'Communicatie',
                                                width: '5%',
                                                options: {1: "<span style='color:red;'>Slecht</span>", 2: "<span style='color:red;'>Onvoldoende</span>", 3: "Voldoende", 4: "Goed", 5: "Zeer Goed"}
                                            },
                                            punt_4: {
                                                title: 'Technische kennis',
                                                width: '15%',
                                                options: {1: "<span style='color:red;'>Slecht</span>", 2: "<span style='color:red;'>Onvoldoende</span>", 3: "Voldoende", 4: "Goed", 5: "Zeer Goed"}
                                            },
                                            punt_5: {
                                                title: 'Kwaliteit Tickets',
                                                width: '15%',
                                                options: {1: "<span style='color:red;'>Slecht</span>", 2: "<span style='color:red;'>Onvoldoende</span>", 3: "Voldoende", 4: "Goed", 5: "Zeer Goed"}
                                            },
                                            punt_6: {
                                                title: 'Proces Kennis',
                                                width: '17%',
                                                options: {1: "<span style='color:red;'>Slecht</span>", 2: "<span style='color:red;'>Onvoldoende</span>", 3: "Voldoende", 4: "Goed", 5: "Zeer Goed"}
                                            },
                                            punt_7: {
                                                title: 'Extra (Opt.)',
                                                width: '7%',
                                                options: {0: "<span style='color:red;'>N.v.t.</span>", 1: "<span style='color:red;'>Slecht</span>", 2: "<span style='color:red;'>Onvoldoende</span>", 3: "Voldoende", 4: "Goed", 5: "Zeer Goed"}
                                            },
                                            totaalscore: {
                                                title: "Totaalscore",
                                                width: "5%",
                                                list: true,
                                                create: false,
                                                edit: false
                                            },
                                            verbeterpunt: {
                                                list: false,
                                                create: true,
                                                edit: true,
                                                title: 'Verbeterpunten',
                                                width: '7%',
                                                type: "textarea"
                                            },
                                            verbeterbutton: {
                                              width: "1%",
                                              sorting: false,
                                              edit: false,
                                              create: false,
                                              listClass: 'child-opener-image-column',
                                              display: function (Data) {
                                                  var $img = $("<img class='child-opener-image' src='{{ URL::to('js/themes/metro/') }}list_metro.png' title='Verbeterpunt' />");
                                                  $img.click(function () {
                                                    alert(Data.record.verbeterpunt);
                                                  });
                                                  return $img;
                                              }
                                            }
                                        }
                                        }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                        });
                        return $img;
                    }
            }
          }
        });
    $('#medewerkerstablecontainer').jtable('load');
    });

</script>
</head>

<!--[if IE 7 ]>    <body class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <body class="ie8"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<body>



<!--<![endif]-->
<div id="wrapper" class="container_12"> 



<!-- start header -->
  <header> 
    <!-- logo -->
    <h1 id="logo"><a href="{{ URL::to('/') }}">KPN</a></h1>
    <!-- nav -->
       <nav>
<ul id="nav">
  <li><a href="{{ URL::to('/') }}">Dashboard</a></li>
  <li class="current"><a href="{{ URL::to('beoordeling') }}">Beoordelingen</a></li>
  <li><a href="{{ URL::to('manager') }}">Managers</a></li>

  <!-- <li><a href="{{ URL::to('uitloggen') }}">Uitloggen</a></li> -->
</ul>
<br class="cl" />
    </nav>
  <br class="cl" />  
  </header>
  
  <div id="page">
  
  <h2 class="ribbon">Beoordelingen</h2>

  <div class="triangle-ribbon"></div>
  <div id="grid">
    <input type="text" id="naam"><input type="submit" value="Vraag op" id="knopje"><br /><br />
    <div id="medewerkerstablecontainer"></div>

  </div>
  </div>
    
  <footer>
    <ul class="footer-nav">
      <li><a href="{{ URL::to('/') }}">Dashboard</a> |</li>
      <li><a href="http://www.kpn.nl">KPN</a></li>
    </ul>
    <p>Copyright &copy;2014, <a href="http://www.danakool.nl">Dana Kool</a></p>
    <br class="cl" />
  </footer>
  
  
  <!--[if lt IE 7 ]>
    {{ HTML::script('js/dd_belatedpng.js') }}
  <![endif]--> 
</div>
</body>
</html>
