<?php


Route::get('/', function(){
    $data = DB::Query("select medewerker.naam, beoordeling.created_at as beoordelingsdatum, (((punt_1*1.5)+(punt_2*1.5)+(punt_3)+(punt_4)+(punt_5*0.5)+(punt_6*0.5))/6/1.25) as totaal from beoordeling inner join medewerker on beoordeling.medewerker_id=medewerker.id where beoordelingsdatum > date('now', '-3 month') group by naam order by totaal desc;");
    return View::make('dashboard.index')->with('data', $data);
});

Route::controller('medewerker');
Route::controller('beoordeling');
Route::controller('manager');
Route::controller('rapport');
Route::controller('scores');
Route::controller('dashboard');

Event::listen('404', function()
{
	return Response::error('404');
});

Event::listen('500', function($exception)
{
	return Response::error('500');
});

//////////////
//////////////


Route::filter('csrf', function()
{
	if (Request::forged()) return Response::error('500');
});

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::to('login');
});