<?php

class Dashboard_Controller extends Base_Controller {

	public function action_index()
	{
		$data = DB::Query("select medewerker.naam, beoordeling.created_at as beoordelingsdatum, (((punt_1*1.5)+(punt_2*1.5)+(punt_3)+(punt_4)+(punt_5*0.5)+(punt_6*0.5))/6/1.25) as totaal from beoordeling inner join medewerker on beoordeling.medewerker_id=medewerker.id where beoordelingsdatum > date('now', '-3 month') group by naam order by totaal desc;");
                return View::make('dashboard.index')->with('data', $data);
	}

}