<?php

class Scores_Controller extends Base_Controller {

	public function action_bereken($manager)
	{
            $manager = urldecode($manager);
            $data = DB::Query("select medewerker.naam, beoordeling.created_at as beoordelingsdatum, (((punt_1*1.5)+(punt_2*1.5)+(punt_3)+(punt_4)+(punt_5*0.5)+(punt_6*0.5))/6/1.25) as totaal from beoordeling inner join medewerker on beoordeling.medewerker_id=medewerker.id where beoordelingsdatum > date('now', '-3 month') and medewerker.manager = '$manager' group by naam order by totaal desc;");
            return View::make('scores.index')->with('manager', $manager)->with('data', $data);
	}

}