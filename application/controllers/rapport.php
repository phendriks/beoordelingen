<?php

class Rapport_Controller extends Base_Controller {

	
        public function action_verwerk($manager, $id)
        {
                $beoordelingen = DB::query('select id,medewerker_id,created_at,punt_1,punt_2,punt_3,punt_4,punt_5,punt_6,punt_7,verbeterpunt from beoordeling where medewerker_id = '.$id);
                
                foreach ($beoordelingen as $beoordeling)
                {
                    if($beoordeling->punt_7 == 0){
                        $punt1 = (float)$beoordeling->punt_1;
                        $punt2 = (float)$beoordeling->punt_2;
                        $punt3 = (float)$beoordeling->punt_3;
                        $punt4 = (float)$beoordeling->punt_4;
                        $punt5 = (float)$beoordeling->punt_5;
                        $punt6 = (float)$beoordeling->punt_6;
                        $beoordeling->totaalscore = round(((($punt1 * 1.5)+($punt2 * 1.5)+($punt3)+($punt4)+($punt5 * 0.5)+($punt6 * 0.5))/6/1.25), 2);
                    }
                    else
                    {
                        $punt1 = (float)$beoordeling->punt_1;
                        $punt2 = (float)$beoordeling->punt_2;
                        $punt3 = (float)$beoordeling->punt_3;
                        $punt4 = (float)$beoordeling->punt_4;
                        $punt5 = (float)$beoordeling->punt_5;
                        $punt6 = (float)$beoordeling->punt_6;
                        $punt7 = (float)$beoordeling->punt_7;
                        $beoordeling->totaalscore = round(((($punt1 * 1.5)+($punt2 * 1.5)+($punt3)+($punt4)+($punt5 * 0.5)+($punt6 * 0.5)+($punt7))/7/1.25), 2);
                    }
                }
                
                
                $naam = DB::query('select naam from medewerker where id = '.$id);
                $response['naam'] = $naam[0]->naam;
                $response['id'] = $id;
                $response['manager'] = urldecode($manager);
                $response['beoordelingen'] = $beoordelingen;
                
                return View::make('rapport.print')->with($response);
        }
        public function action_chart($id){
                $return = array();
                $item = array();
                $beoordelingen = DB::query('select strftime("%Y-%m-%d",created_at) as created_at,punt_1,punt_2,punt_3,punt_4,punt_5,punt_6,punt_7 from beoordeling where medewerker_id = '.$id);
                foreach ($beoordelingen as $beoordeling){
                    $item['label'] = $beoordeling->created_at;
                    $item['spider']['show'] = true;
                    $item['spider']['fill'] = true;
                    $temp = array();
                    $punt1[0] = 0;
                    $punt1[1] = (int)$beoordeling->punt_1 *10;
                    array_push($temp, $punt1);
                    $punt2[0] = 1;
                    $punt2[1] = (int)$beoordeling->punt_2 *10;
                    array_push($temp, $punt2);
                    $punt3[0] = 2;
                    $punt3[1] = (int)$beoordeling->punt_3 *10;
                    array_push($temp, $punt3);
                    $punt4[0] = 3;
                    $punt4[1] = (int)$beoordeling->punt_4 *10;
                    array_push($temp, $punt4);
                    $punt5[0] = 4;
                    $punt5[1] = (int)$beoordeling->punt_5 *10;
                    array_push($temp, $punt5);
                    $punt6[0] = 5;
                    $punt6[1] = (int)$beoordeling->punt_6 *10;
                    array_push($temp, $punt6);
                    $punt7[0] = 6;
                    $punt7[1] = (int)$beoordeling->punt_7 *10;
                    array_push($temp, $punt7);
                    $item['data'] = $temp;
                    array_push($return, $item);
                }
                
                
                return Response::json($return);
        }
}