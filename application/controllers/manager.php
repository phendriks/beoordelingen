<?php

class Manager_Controller extends Base_Controller {

	public function action_index()
	{
		return View::make('managers.show');
	}

	public function action_update(){
		// Alle gegevens die doorgegeven zijn met de post van jtable afvangen en in apparte variabele stoppen voor het overzicht.
		$input = Input::all(); // alles in een array gestopt.
		
		$id = $input['id'];
		$managerinput = $input['manager'];

		// Medewerker instantieren om een row te wijzigen.
		$manager = Manager::find($id);
		$manager->manager = $managerinput;
		$manager->save();

		Return Response::json(array('Result' => 'OK'));
	}

	public function action_add(){
		// Alle gegevens die doorgegeven zijn met de post van jtable afvangen en in apparte variabele stoppen voor het overzicht.
		$input = Input::all(); // alles in een array gestopt.
		
		$managerinput = $input['manager'];
		

		// Medewerker instantieren om een row te wijzigen.
		$manager = new Manager();
		$manager->manager = $managerinput;
		$manager->save();
		// niet meer zeker of er een antwoord moet terug komen van jtable maar anders voor het geval dat een OK sturen.
		Return Response::json(array('Result' => 'OK', 'Record' => $input));
	}

	public function action_delete(){
		// afvangen input in variabele id
		$id = Input::get('id');
		// id waarop gezocht wordt
		$manager = Manager::find($id);
		$manager->delete();

		Return Response::json(array('Result' => 'OK'));
	}

	public function action_list(){
		
		// $order = Input::get('jtSorting');
		// $start = Input::get('jtStartIndex');
		// $pagesize = Input::get('jtPageSize');
		$managers = array();
		
		// $table['Result'] = 'ERROR';
		
			$managers = DB::query('select * from managers ORDER BY manager ASC');
			$table['Result'] = 'OK';
			$table['TotalRecordCount'] = DB::table('managers')->count();
			$table['Records'] = $managers;
		// }
		// else {
			// $table['Message'] = "Er gaat iets niet helemaal goed in de aanvraag";
		// }
		
		Return Response::json($table);
	}

}