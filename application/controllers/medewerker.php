<?php

class Medewerker_Controller extends Base_Controller{

	public function action_index(){
		Return View::make('beoordelingen.overzicht.show');
	}

	public function action_update(){
		// Alle gegevens die doorgegeven zijn met de post van jtable afvangen en in apparte variabele stoppen voor het overzicht.
		$input = Input::all(); // alles in een array gestopt.
		
		$id = $input['id'];
		$naam = $input['naam'];
		$manager = $input['manager'];

		// Medewerker instantieren om een row te wijzigen.
		$medewerker = Medewerker::find($id);
		$medewerker->naam = $naam;
		$medewerker->manager = $manager;
		$medewerker->save();

		Return Response::json(array('Result' => 'OK'));
	}

	public function action_add(){
		// Alle gegevens die doorgegeven zijn met de post van jtable afvangen en in apparte variabele stoppen voor het overzicht.
		$input = Input::all(); // alles in een array gestopt.
		
		$naam = $input['naam'];
		$manager = $input['manager'];
		

		// Medewerker instantieren om een row te wijzigen.
		$medewerker = new Medewerker();
		$medewerker->naam = $naam;
		$medewerker->manager = $manager;
		$medewerker->save();
		// niet meer zeker of er een antwoord moet terug komen van jtable maar anders voor het geval dat een OK sturen.
		Return Response::json(array('Result' => 'OK', 'Record' => $input));
	}

	public function action_delete(){
		// afvangen input in variabele id
		$id = Input::get('id');
		// id waarop gezocht wordt
		$medewerker = Medewerker::find($id);
		$medewerker->delete();

		Return Response::json(array('Result' => 'OK'));
	}

	public function action_list(){
		
		$order = Input::get('jtSorting');
		$start = Input::get('jtStartIndex');
		$pagesize = Input::get('jtPageSize');
		

		$naam = Input::get('naam');
		$medewerkers = array();
		
		$table['Result'] = 'ERROR';
		
		if($order != null && $start != null && $pagesize != null) {
			if(!empty($naam)){
				$medewerkers = DB::query('select * from medewerker where naam like "%'.$naam.'%"');	
			}
			else{
				$medewerkers = DB::query('select * from medewerker ORDER BY '.$order.' LIMIT '.$start.','.$pagesize.'');	
			}
			
			$table['Result'] = 'OK';
			$table['TotalRecordCount'] = DB::table('medewerker')->count();
			$table['Records'] = $medewerkers;
		}
		else {
			$table['Message'] = "Er gaat iets niet helemaal goed in de aanvraag";
		}
		
		Return Response::json($table);
	}

	public function action_managers(){
		$managers = DB::query('select manager from managers order by manager ASC');
		$retArray['Options'] = array();
		foreach ($managers as $key => $value) {
			$temp['DisplayText'] = $value->manager;
			$temp['Value'] = $value->manager;
			array_push($retArray['Options'], $temp);
		}
		$retArray['Result'] = 'OK';

		Return Response::json($retArray);
	}
}