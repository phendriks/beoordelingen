<?php

class Beoordeling_Controller extends Base_Controller {

	public function action_index()
	{
		return View::make('beoordelingen.overzicht.show');
	}

	public function action_update(){
		// Alle gegevens die doorgegeven zijn met de post van jtable afvangen en in apparte variabele stoppen voor het overzicht.
		
		$input = Input::all(); // alles in een array gestopt.
		
		$id = $input['id'];
		$punt_1 = $input['punt_1'];
		$punt_2 = $input['punt_2'];
		$punt_3 = $input['punt_3'];
		$punt_4 = $input['punt_4'];
		$punt_5 = $input['punt_5'];
		$punt_6 = $input['punt_6'];
                $punt_7 = $input['punt_7'];
		$verbeterpunt = $input['verbeterpunt'];		

		// Medewerker instantieren om een row te wijzigen.
		$beoordeling = Beoordeling::find($id);
		
		$beoordeling->punt_1 = $punt_1;
		$beoordeling->punt_2 = $punt_2;
		$beoordeling->punt_3 = $punt_3;
		$beoordeling->punt_4 = $punt_4;
		$beoordeling->punt_5 = $punt_5;
		$beoordeling->punt_6 = $punt_6;
                $beoordeling->punt_7 = $punt_7;
		$beoordeling->verbeterpunt = $verbeterpunt;
		$beoordeling->save();

		Return Response::json(array('Result' => 'OK'));
	}

	public function action_add($id){
		// Alle gegevens die doorgegeven zijn met de post van jtable afvangen en in apparte variabele stoppen voor het overzicht.
		$input = Input::all(); // alles in een array gestopt.
		
		$punt_1 = $input['punt_1'];
		$punt_2 = $input['punt_2'];
		$punt_3 = $input['punt_3'];
		$punt_4 = $input['punt_4'];
		$punt_5 = $input['punt_5'];
		$punt_6 = $input['punt_6'];
                $punt_7 = $input['punt_7'];
		$verbeterpunt = $input['verbeterpunt'];
		

		// Medewerker instantieren om een row te wijzigen.
		$beoordeling = new Beoordeling();
		$beoordeling->medewerker_id = $id;
		$beoordeling->punt_1 = $punt_1;
		$beoordeling->punt_2 = $punt_2;
		$beoordeling->punt_3 = $punt_3;
		$beoordeling->punt_4 = $punt_4;
		$beoordeling->punt_5 = $punt_5;
		$beoordeling->punt_6 = $punt_6;
                $beoordeling->punt_7 = $punt_7;
		$beoordeling->verbeterpunt = $verbeterpunt;
		$beoordeling->save();

		$response = DB::table('beoordeling')->order_by('id', 'desc')->first();
		// niet meer zeker of er een antwoord moet terug komen van jtable maar anders voor het geval dat een OK sturen.
		Return Response::json(array('Result' => 'OK', 'Record' => $response));
	}

	public function action_delete(){
		// afvangen input in variabele id
		$id = Input::get('id');
		// id waarop gezocht wordt
		$beoordeling = Beoordeling::find($id);
		$beoordeling->delete();

		Return Response::json(array('Result' => 'OK'));
	}

	public function action_list($id){
		
		// $order = Input::get('jtSorting');
		// $start = Input::get('jtStartIndex');
		// $pagesize = Input::get('jtPageSize');
		$beoordelingen = array();
		$result;
		// $table['Result'] = 'ERROR';
		
		// if($order != null && $start != null && $pagesize != null) {
			$beoordelingen = DB::query('select id,medewerker_id,created_at,punt_1,punt_2,punt_3,punt_4,punt_5,punt_6,punt_7,verbeterpunt, round((((punt_1 * 1.5)+(punt_2 * 1.5)+(punt_3)+(punt_4)+(punt_5 * 0.5)+(punt_6 * 0.5)+(punt_7 * 1))/7/1.25),2) as Totaalscore from beoordeling where medewerker_id = '.$id.' ORDER BY created_at DESC');
			foreach ($beoordelingen as $beoordeling)
                        {
                            if($beoordeling->punt_7 == 0){
                                $punt1 = (float)$beoordeling->punt_1;
                                $punt2 = (float)$beoordeling->punt_2;
                                $punt3 = (float)$beoordeling->punt_3;
                                $punt4 = (float)$beoordeling->punt_4;
                                $punt5 = (float)$beoordeling->punt_5;
                                $punt6 = (float)$beoordeling->punt_6;
                                $beoordeling->totaalscore = round(((($punt1 * 1.5)+($punt2 * 1.5)+($punt3)+($punt4)+($punt5 * 0.5)+($punt6 * 0.5))/6/1.25), 2);
                            }
                            else
                            {
                                $punt1 = (float)$beoordeling->punt_1;
                                $punt2 = (float)$beoordeling->punt_2;
                                $punt3 = (float)$beoordeling->punt_3;
                                $punt4 = (float)$beoordeling->punt_4;
                                $punt5 = (float)$beoordeling->punt_5;
                                $punt6 = (float)$beoordeling->punt_6;
                                $punt7 = (float)$beoordeling->punt_7;
                                $beoordeling->totaalscore = round(((($punt1 * 1.5)+($punt2 * 1.5)+($punt3)+($punt4)+($punt5 * 0.5)+($punt6 * 0.5)+($punt7))/7/1.25), 2);
                            }
                        }
                        $table['Result'] = 'OK';
			$result = DB::query('select count(*) as count from beoordeling where medewerker_id = '.$id.'');
			$table['TotalRecordCount'] = $result[0]->count;
			$table['Records'] = $beoordelingen;
		// }
		// else {
		// 	$table['Message'] = "Er gaat iets niet helemaal goed in de aanvraag";
		// }
		
		Return Response::json($table);
	}
}
