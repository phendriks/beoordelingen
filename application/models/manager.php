<?php

class Manager extends Eloquent {

	public static $table = 'managers';
	public static $timestamps = false;
}